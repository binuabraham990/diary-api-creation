<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NoteController extends Controller {

    public function saveNoteAction(Request $request) {

        $subject = $request->request->get('subject');
        $content = $request->request->get('content');
        $guid = $request->request->get('secret');
        $token = $request->request->get('token');
        $em = $this->getDoctrine()->getManager();
        $type = 'danger';
        $msg = '';
        if ($subject == '' || $content == '') {
            $msg = 'Please insert all fields';
        } elseif ($guid == '') {
            $msg = 'Oops something weird happened. Please restart your application';
        } elseif ($token == '' || !$this->container->get('app.services')->checkToken($token)) {
            $msg = 'Unathorized request';
        } else {
            $user = $this->container->get('app.services')->checkUser($guid);
            if ($user) {
                $guid = md5(uniqid(php_uname('n')));
                $today = new \DateTime();
                $note = new \AppBundle\Entity\Note();
                $note->setUser($user);
                $note->setGuid($guid);
                $note->setSubject($subject);
                $note->setUpdateTime($today->format('Y-m-d'));
                $note->setContent($content);
                $em->persist($note);
                $em->flush();
                return new JsonResponse(['type' => 'success', 'msg' => 'Note Saved']);
            } else {
                $msg = 'Unauthenticated entry';
            }
        }
        if ($msg != '') {
            return new JsonResponse(['type' => $type, 'msg' => $msg]);
        }
    }

    public function getNotesAction(Request $request) {

        $guid = $request->request->get('secret');
        $date = $request->request->get('date');

        if ($guid == '' || $date == '') {
            return new JsonResponse(['type' => 'danger', 'msg' => 'Please insert date.']);
        } else {
            $em = $this->getDoctrine()->getManager();
            $notes = $em->createQueryBuilder('n')
                    ->select('n.subject, n.content')
                    ->from('AppBundle:Note', 'n')
                    ->leftJoin('n.user', 'u')
                    ->where('n.updateTime = :date')
                    ->andWhere('u.guid = :guid')
                    ->setParameters(['guid' => $guid, 'date' => $date])
                    ->getQuery()
                    ->getResult();
            
            return new JsonResponse(['type' => 'success', 'msg' => 'Fetched details.', 'notes' => json_encode($notes, 128)]);
        }
    }

}
