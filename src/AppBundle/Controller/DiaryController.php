<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DiaryController extends Controller {

    public function showRegAction() {

        $token = $this->container->get('app.services')->getToken();
        return new JsonResponse(['token' => $token]);
    }

    public function getKeyAction(Request $request) {
        $token = $request->request->get('token');
        $email = $request->request->get('email');
        $password = $request->request->get('pass');
        $type = 'danger';
        $msg = $this->checkErrorInRegAndLoginInput($token, $email, $password);

        if ($msg == '') {
            $em = $this->getDoctrine()->getManager();
            $guid = md5(uniqid(php_uname('n')));
            $user = new \AppBundle\Entity\Guys();
            $user->setEmail($email);
            $user->setPassword(md5($password));
            $user->setSalt(md5('uppu'));
            $user->setGuid($guid);
            $em->persist($user);
            $em->flush();
            return new JsonResponse(['type' => 'success', 'msg' => 'Catch your id man.', 'secret' => $guid]);
        } else {
            return new JsonResponse(['type' => $type, 'msg' => $msg]);
        }
    }

    public function loginAction(Request $request) {
        $token = $request->request->get('token');
        $email = $request->request->get('email');
        $password = $request->request->get('pass');
        $type = 'danger';
        $msg = $this->checkErrorInRegAndLoginInput($token, $email, $password);
        
        if($msg == '')  {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Guys')->findOneBy(['email'=> $email, 'password' => md5($password)]);
            if($user)   {
                $guid = $user->getGuId();
                return new JsonResponse(['type' => 'success', 'msg' => 'You are successfully logged in.', 'secret' => $guid]);
            }   else    {
                $msg = 'You entered some wrong values.';
            }
        }
        return new JsonResponse(['type' => $type, 'msg' => $msg]);
    }

    private function checkErrorInRegAndLoginInput($token, $email, $password) {
        
        $msg = '';
        if ($token != "" && $email != "" && $password != "") {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $msg = 'I think you type mail id incorrect';
            }

            if (strlen($password) < 6) {
                $msg = 'Man make your password little big. I need a minimum 6 chars';
            }
        } else {
            $msg = 'Boss looks like you forgot something...!';
        }
        $tokenStatus = $this->container->get('app.services')->checkToken($token);

        if (!$tokenStatus) {
            $msg = 'Unathorized request';
        }
        
        return $msg;
    }

    public function gototwigAction() {

        $token = $this->container->get('app.services')->getToken();
        return $this->render('AppBundle:Default:registration.html.twig', compact('token'));
    }

}
