<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Session\Session;

class appServices {
    
    private $session;
    private $container;
    private $em;
    
    public function __construct($container, $em) {
        $this->container = $container;
        $this->session = $this->container->get('session');
        $this->em = $em;
    }
    
    public function createSession() {
        if($this->container && (!$this->container->get('session')->isStarted()))  {
            $newSession = new Session();
            $newSession->start();
            return $newSession;
        } else {
            return $this->container->get('session');
        }
    }
    
    public function getToken() {
        
        $this->sesstion = $this->createSession();
        $token = md5(uniqid(php_uname('n')));
        $this->sesstion->set('token', $token);
        return $token;
    }
    
    public function checkToken($token) {
        
        $this->sesstion = $this->createSession();
        $sessionToken = $this->sesstion->get('token');
        if($token)  {
            if($token == $sessionToken)    {
                return TRUE;
            }   else    {
                return FALSE;
            }
        }  
    }
    
    public function checkUser($guid)    {
        
        $user = $this->em->getRepository('AppBundle:Guys')->findOneByGuid($guid);
        if($user)   {
            return $user;
        }   else    {
            return FALSE;
        }
    }
}
